# The Halo Project
![Python 3.6+ supported](https://img.shields.io/badge/python-3.6%2B-yellow?style=flat&logo=python "Python versions supported")

## What it's suposed to be
A support AI I decided to create after craving friendly advice and hugs. Inspired by a visual novel I played called "//TODO: today", which by the way, it's free [on Steam](https://store.steampowered.com/app/1259240/).

## Features planned

- GUI interaction with a virtual avatar
- Speech recognition
- Text-to-speech output
- TensorFlow-powered Deep Learning (this branch)
- PyTorch-powered Neural Networks (the other branch)
- OS control (auto suspend/on/off, open apps)?
- some features from most assistants nowadays (set reminders/alarms, check schedule, etc.)?

## Installation
No installation yet.

## Usage

- Clone the repository and browse to it.
- Install the requirements with `pip install -r requirements.txt`.
- After all the requirements install, run it with `python halo.py`.
- Enjoy!

## Support
"Tell people where they can go to for help." As if developers don't know how to use their own framework-- jk jk, there's nothing here yet.

## Roadmap
I wanna release this AI in a working state, with major working features, in around 2 years or so. Let's see how well this promise ages. XD

## Contributing
If you want to contribute, that's fine! I'd love some help, especially because I don't know what to do myself lol

## License
The project is licensed under the MIT license. So whatever you do, just don't sue me. LMAO

## Project status
> "If you have run out of energy or time for your project..."

Bold of you to assume I have any of those things in life, GitLab lmao
