import numpy as np
import tensorflow as tf
import pickle
from tensorflow.keras import layers, activations, models, preprocessing, utils
import os
import yaml
# from gensim import Word2Vec
import re

#####################################
# DATA AND VOCABULARY PREPROCESSING #
#####################################

# Define our data directory
datapath = 'chatterbot_english/data'
filelist = os.listdir(datapath + os.sep)

# Initialize question and answer lists
questions = list()
answers = list()

# Parse each file.
for file in filelist:
    # Read file as binary stream in read mode.
    fstream = open(datapath + os.sep + file, "rb")
    # Use stream as YAML.
    docs = yaml.safe_load(fstream)
    conversation = docs['conversations']
    for con in conversation:
        # If question has multiple answers.
        if len(con) > 2:
            questions.append(con[0])
            replies = con[1:]
            ans = ""
            for rep in replies:
                ans += " " + rep
            answers.append(ans)
        # If question has a single answer.
        elif len(con) > 1:
            questions.append(con[0])
            answers.append(con[1])

# Initialize list of tagged answers.
tagged_answers = list()
# Iterate through the answer list.
for i in range(len(answers)):
    # If answer is a string.
    if type(answers[i]) == str:
        tagged_answers.append(answers[i])
    # If answer is a number or other type, remove from questions.
    else:
        questions.pop(i)

# Reinitialize the answer list.
answers = list()
# Iterate through tagged answers list, append tags to post-process answers.
for i in range(len(tagged_answers)):
    answers.append("<START> " + tagged_answers[i] + " <END>")

# Initialize tokenizer.
tokenizer = preprocessing.text.Tokenizer()
# Process questions and answers.
tokenizer.fit_on_texts(questions + answers)
# Get length of vocabulary.
vocab_size = len(tokenizer.word_index) + 1
print("Vocabulary size: {}".format(vocab_size))

#####################################
# DATA PREPARATION FOR SEQ2SEQ      #
#####################################

# Initialize vocabulary.
vocab = []
# Append our words to the vocabulary.
for word in tokenizer.word_index:
    vocab.append(word)

# Tokenize our sentences.
def tokenize(sentences):
    tokens_list = []
    vocabulary = []
    for sentence in sentences:
        sentence = sentence.lower()
        sentence = re.sub('[^a-zA-Z]', ' ', sentence)
        tokens = sentence.split()
        vocabulary += tokens
        tokens_list.append(tokens)
    return tokens_list, vocabulary

# Process encoder input data as an array of numbers to work with the RNN.
tokenized_questions = tokenizer.texts_to_sequences(questions)
maxlen_questions = max([len(x) for x in tokenized_questions])
padded_questions = preprocessing.sequence.pad_sequences(tokenized_questions, maxlen=maxlen_questions, padding='post')
encoder_input_data = np.array(padded_questions)
print("Encoder input data:\n-------------------------")
print(encoder_input_data.shape, maxlen_questions)

# Process decoder input data as an array of numbers to work with the RNN.
tokenized_answers = tokenizer.texts_to_sequences(answers)
maxlen_answers = max([len(x) for x in tokenized_answers])
padded_answers = preprocessing.sequence.pad_sequences(tokenized_answers, maxlen=maxlen_answers, padding='post')
decoder_input_data = np.array(padded_answers)
print("Decoder input data:\n-------------------------")
print(decoder_input_data.shape, maxlen_answers)

# Process decoder output data
tokenized_answers = tokenizer.texts_to_sequences(answers)
for i in range(len(tokenized_answers)):
    tokenized_answers[i] = tokenized_answers[i][1:]
padded_answers = preprocessing.sequence.pad_sequences(tokenized_answers, maxlen=maxlen_answers, padding='post')
onehot_answers = utils.to_categorical(padded_answers, vocab_size)
decoder_output_data = np.array(onehot_answers)
print("Decoder output data:\n------------------------")
print(decoder_output_data.shape)

#####################################
# DEFINE AND COMPILE MODEL          #
#####################################

# Define inputs, embedding, Long-Short-Term Memory and states for encoder.
encoder_inputs = tf.keras.layers.Input(shape=(maxlen_questions, ))
encoder_embedding = tf.keras.layers.Embedding(vocab_size, 200, mask_zero=True) (encoder_inputs)
encoder_outputs, state_h, state_c = tf.keras.layers.LSTM(200, return_state=True) (encoder_embedding)
encoder_states = [state_h, state_c]

# Define inputs, embedding, Long-Short-Term Memory and states for decoder. Also outputs.
decoder_inputs = tf.keras.layers.Input(shape=(maxlen_answers, ))
decoder_embedding = tf.keras.layers.Embedding(vocab_size, 200, mask_zero=True) (decoder_inputs)
decoder_lstm = tf.keras.layers.LSTM(200, return_state=True, return_sequences=True)
decoder_outputs, _, _ = decoder_lstm(decoder_embedding, initial_state=encoder_states)
decoder_dense = tf.keras.layers.Dense(vocab_size, activation=tf.keras.activations.softmax)
output = decoder_dense(decoder_outputs)

# Define and compile model (finally!)
model = tf.keras.models.Model([encoder_inputs, decoder_inputs], output)
model.compile(optimizer=tf.keras.optimizers.RMSprop(), loss='categorical_crossentropy')

# Print a summary of the model (I think?)
model.summary()

#####################################
# TRAINING/LOADING THE MODEL        #
#####################################

# Finally training the model.
model.fit([encoder_input_data, decoder_input_data], decoder_output_data, batch_size=50, epochs=150)
model.save('model.h5')
#model = tf.keras.models.load_model('model.h5')

#####################################
# DEFINING INFERENCE MODELS         #
#####################################

# Define the inference models.
def make_inference_models():
    # First, the encoder inference model.
    encoder_model = tf.keras.models.Model(encoder_inputs, encoder_states)

    # Then, the decoder model, along with states and necessary data.
    decoder_state_input_h = tf.keras.layers.Input(shape=(200, ))
    decoder_state_input_c = tf.keras.layers.Input(shape=(200, ))

    decoder_states_inputs = [decoder_state_input_h, decoder_state_input_c]

    decoder_outputs, state_h, state_c = decoder_lstm(decoder_embedding, initial_state=decoder_states_inputs)
    decoder_states = [state_h, state_c]
    decoder_outputs = decoder_dense(decoder_outputs)
    decoder_model = tf.keras.models.Model([decoder_inputs] + decoder_states_inputs, [decoder_outputs] + decoder_states)

    # Return both models.
    return encoder_model, decoder_model

#####################################
# CONVERT STRING TO TOKENS          #
#####################################

def sentence_to_tokens(sentence: str):
    # Removes symbols from our sentence.
    stripped_sentence = re.sub(r'[^\w]', '', sentence)
    # Convert our sentence to lowercase, then split.
    words = stripped_sentence.lower().split()
    # Generate an empty list for our tokens.
    token_list = list()
    # Append each word index to token list.
    for word in words:
        token_list.append(tokenizer.word_index[word])
    # Return preprocessed, padded sequence.
    return preprocessing.sequence.pad_sequences([token_list], maxlen=maxlen_questions, padding='post')

#####################################
#            MAIN LOOP              #
#####################################

# Create our inference models.
enc_model, dec_model = make_inference_models()

for _ in range(10):
    states_values = enc_model.predict(sentence_to_tokens(input("> ")))
    empty_target_seq = np.zeros((1, 1))
    empty_target_seq[0, 0] = tokenizer.word_index['start']
    stop_condition = False
    decoded_translation = ''
    while not stop_condition:
        dec_outputs, h, c = dec_model.predict([empty_target_seq] + states_values)
        sampled_word_index = np.argmax(dec_outputs[0, -1, :])
        sampled_word = None
        for word, index in tokenizer.word_index.items():
            if sampled_word_index == index:
                decoded_translation += ' {}'.format(word)
                sampled_word = word
        # If the predicted word is end or the sentence surpasses the max length for answers, stop
        if sampled_word == 'end' or len(decoded_translation.split()) > maxlen_answers:
            stop_condition = True
        
        empty_target_seq = np.zeros((1, 1))
        empty_target_seq[0, 0] = sampled_word_index
        states_values = [h, c]

    print("< {}".format(decoded_translation))

converter = tf.lite.TFLiteConverter.from_keras_model(enc_model)
buffer = converter.convert()
open("enc_model.tflite", "wb").write(buffer)

converter = tf.lite.TFLiteConverter.from_keras_model(dec_model)
buffer = converter.convert()
open("dec_model.tflite", "wb").write(buffer)
